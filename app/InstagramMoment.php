<?php namespace MaajiRealMermaids;

/**
 * Created by androide_osorio.
 * Date: 5/20/15
 * Time: 16:58
 */
class InstagramMoment extends Moment {

    /**
     * guarded attributes from mass assignment
     * @var array
     */
    protected $guarded = ['id'];
}