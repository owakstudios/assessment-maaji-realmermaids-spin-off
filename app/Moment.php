<?php namespace MaajiRealMermaids;

use MaajiRealMermaids\Utilities\Traits\SingleTableInheritanceTrait;
use Illuminate\Database\Eloquent\Model;

class Moment extends Model {

    use SingleTableInheritanceTrait;

    //--------------------------------------------------------------

    /**
     * the main moments table
     * @var string
     */
    protected $table = 'moments';

    /**
     * table does not have auto-incrementing id
     * @var boolean
     */
    public $incrementing = false;

    //--------------------------------------------------------------

    public function __construct(array $attributes = array())
    {
        parent::__construct($attributes);
        $this->setAttribute('type', $this->getClassName(false));
    }

    /**
     * The "booting" method of the model.
     *
     * @return void
     */
    protected static function boot()
    {
        parent::boot();

        /**
         * Attach to the 'creating' Model Event to provide a UUID
         * for the `id` field (provided by $model->getKeyName())
         */
        static::creating( function ($model) {
            $primary_key = $model->getKeyName();

            if ( empty( $model->{$primary_key} ) ) {
                $model->{$primary_key} = (string)Uuid::uuid4();
            }
        } );
    }

    //--------------------------------------------------------------

    /**
     * a moment belongs to a user
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function owner()
    {
        return $this->belongsTo('MaajiRealMermaids\User');
    }
}
