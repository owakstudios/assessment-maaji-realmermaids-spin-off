<?php namespace MaajiRealMermaids\Repositories;

/**
 * Created by androide_osorio.
 * Date: 5/20/15
 * Time: 15:49
 */

use MaajiRealMermaids\User;

class UsersRepository {

    /**
     * finds a user by their username or creates the user if it does not exist
     * @param $userData
     * @param $provider
     *
     * @return static
     */
    public function findByUsernameOrCreate($userData, $provider)
    {
        $user = User::where('provider_id', '=', $userData->id)->first();

        if(!$user) {
            $userInfo = [
                'name'        => $userData->getName(),
                'username'    => $userData->getNickname(),
                'email'       => $userData->getEmail(),
                'avatar'      => $userData->getAvatar(),
                'provider_id' => $userData->id,
                'provider'    => $provider
            ];

            $user = $this->create( $userInfo );
        }

        return $user;
    }

    /**
     * @param $userData
     *
     * @return static
     */
    public function create($userData)
    {
        return User::create( $userData );
    }
}