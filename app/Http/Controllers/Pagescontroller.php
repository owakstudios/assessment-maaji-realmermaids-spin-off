<?php namespace MaajiRealMermaids\Http\Controllers;

/**
 * Created by androide_osorio.
 * Date: 5/21/15
 * Time: 10:00
 */

use \Auth;

class PagesController extends Controller {

    /**
     * render the moments gallery
     * @return $this
     */
    public function index()
    {
        $user = Auth::user();

        return view('pages.index')->with('user', $user);
    }

    /**
     * render the uploads page
     * @return \Illuminate\View\View
     */
    public function uploadPage()
    {
        return view('pages.upload');

    }

    /**
     * render the instagram sharing page
     * @return $this
     */
    public function instagramSharePage()
    {
        $user = Auth::user();

        return view('pages.instagram-share')->with('user', $user);
    }

    /**
     * render the terms and conditions
     * @return \Illuminate\View\View
     */
    public function termsPage()
    {
        return view('legals.terms-and-conditions');
    }

    /**
     * render the privacy policy page
     * @return \Illuminate\View\View
     */
    public function pricacyPolicyPage()
    {
        return view('legals.privacy-policy');

    }
}