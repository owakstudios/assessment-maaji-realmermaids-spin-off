<?php namespace MaajiRealMermaids\Http\Controllers\Api;

/**
 * Created by androide_osorio.
 * Date: 5/20/15
 * Time: 17:22
 */
use Illuminate\Pagination\Paginator;
use Illuminate\Http\Request;

use MaajiRealMermaids\InstagramMoment;
use MaajiRealMermaids\Http\Controllers\Controller;

class InstagramMomentsController extends Controller {

    /**
     * list all the instagram moments available
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function index(Request $request)
    {
        $moments = InstagramMoment::all();

        if($request->has('per_page')) {
            if($request->has('page')) {
                $currentPage = $request->get('page');
                Paginator::currentPageResolver(function() use ($currentPage)
                {
                    return $currentPage;
                });
            }
            $moments = InstagramMoment::paginate($request->get('per_page'));
        }

        return $moments;
    }

    /**
     * return a single instragram moment
     *
     * @param \MaajiRealMermaids\InstagramMoment $moment
     *
     * @return \Illuminate\Http\Response
     *
     */
    public function show(InstagramMoment $moment)
    {
        return $moment;
    }
}