<?php namespace MaajiRealMermaids;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

class User extends Model implements AuthenticatableContract, CanResetPasswordContract {

	use Authenticatable, CanResetPassword;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'users';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['username', 'email', 'avatar', 'name', 'provider', 'provider_id'];

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = ['password', 'remember_token'];

	//--------------------------------------------------------------

	/**
	 * a user has many moments
	 * @return \Illuminate\Database\Eloquent\Relations\HasMany
	 */
	public function moments()
	{
		return $this->hasMany('MaajiRealMermaids\Moment');
	}

	/**
	 * a user has many moments
	 * @return \Illuminate\Database\Eloquent\Relations\HasMany
	 */
	public function instagramMoments()
	{
		return $this->hasMany('MaajiRealMermaids\InstagramMoment');
	}

}
