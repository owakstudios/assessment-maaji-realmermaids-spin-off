<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Document</title>
</head>
<body>
PRIVACY POLICY

Maaji swimwear cares about your privacy. This privacy policy tells you what information we collect at our websites and how we use it. If you choose to visit any of the Maaji swimwear websites, your visit is subject to this privacy policy, and you agree to the terms of it.

PRIVACY POLICY CHANGES

We reserve the right to modify, alter or otherwise update this Privacy Policy at any time, so we encourage you to review this policy from time to time. Changes to this privacy policy are effective at the time they are posted and your continued use of our sites after posting will constitute acceptance of, and agreement to be bound by, those changes.

- See more at: http://maajiswimwear.com/privacy-policy#sthash.u4BABGKl.dpuf
</body>
</html>